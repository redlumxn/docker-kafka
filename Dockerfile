FROM centos:centos6

ENV APPS_DIR /apps
ENV KAFKA_FILE_NAME kafka_2.10-0.8.2.0
ENV SCPATH /etc/supervisor/conf.d

RUN curl https://bootstrap.pypa.io/ez_setup.py -o - | python
RUN yum install -y tar java 
RUN easy_install pip
RUN pip install supervisor

# Supervisor
RUN mkdir -p /var/log/supervisor
ADD ./supervisord/conf.d/* $SCPATH/

# Download and install Kafka
RUN mkdir -p $APPS_DIR
WORKDIR $APPS_DIR
RUN curl -O http://apache.mirror.serversaustralia.com.au/kafka/0.8.2.0/$KAFKA_FILE_NAME.tgz
RUN tar -xzf $KAFKA_FILE_NAME.tgz -C $APPS_DIR
RUN mv $APPS_DIR/$KAFKA_FILE_NAME $APPS_DIR/kafka
RUN rm -rf $APPS_DIR/kafka/config/server.properties
ADD ./kafka-config/*.properties $APPS_DIR/kafka/config/

# Expose both ports
EXPOSE 2181 9092 9001

#CMD ["/bin/sh", "-c", "$APPS_DIR/kafka/bin/zookeeper-server-start.sh $APPS_DIR/kafka/config/zookeeper.properties"]

CMD ["/bin/sh", "-c" , "supervisord -n -c $SCPATH/supervisord.conf"]
